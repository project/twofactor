
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers



INTRODUCTION
------------

This module integrate a secure login with webauthn from 2factor.app.
We also have created a drush command, if you need to deactivate the 2factor.app for a single user.

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/twofactor



REQUIREMENTS
------------

You need to register an account on 2factor.app to use this module.



Recommended modules
-------------------

To be secure, use also a module that log outs the user after some time.


INSTALLATION
------------

 * Install then the module



CONFIGURATION
-------------


 * Go to 2factor.app
    - Create an account on 2factor.app
    - Add there the application
    - Add there the authentications
 * Inside Drupal, after install the module
    - Set the permission for who you want allow the usage
    - On user profile, go to 2factor.app and put the settings
    - Now you need to authenticate on every login over 2factor.app



TROUBLESHOOTING
---------------

  * Check the settings



MAINTAINERS
-----------

Current maintainers:
  * Ursin Cola (cola) https://www.drupal.org/u/cola

This project has been sponsored by:
 * soul.media
   Drupal agency, www.soul.media
