<?php
namespace Drupal\twofactor\EventSubscriber;

use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\user\UserData;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Routing\RouteMatchInterface;

class CheckTwofactorSubscriber implements EventSubscriberInterface {

  protected $tempStore;
  protected $userData;
  protected $currentUser;
  protected $currentRouteMatch;

  public function __construct(UserData $user_data, AccountProxyInterface $account, RouteMatchInterface $current_route_match) {
    $this->userData = $user_data;
    $this->currentUser = $account;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkTwofactor');
    return $events;
  }

  public function checkTwofactor($event) {
    $baseUrl = $event->getRequest()->getBaseUrl();

    // Check if user is logged in.
    if ($this->currentUser->id() > 0) {
      $uid = $this->currentUser->id();
      $session = $event->getRequest()->getSession();

      // Check if twofactor is enabled.
      if ($this->userData->get('twofactor', $uid, 'status')) {
        // Check if user has autorized twofactor for this login-session.
        if ($session->get('twofactor.allowed') == 0) {
          $redirect = TRUE;

          // Define ignoring routes.
          $ignore = [
            'system.css_asset',
            'system.js_asset',
            'twofactor.user_auth',
            'user.logout',
            'user.logout.confirm',
            'system.403',
          ];

          // Check if current route is in ignore list.
          if (in_array($this->currentRouteMatch->getRouteName(), $ignore)) {
            $redirect = FALSE;
          }

          // Check if Whitelisted IP Adresses are set and yours
          $ip = str_replace(' ', '', $this->userData->get('twofactor', $uid, 'ip'));
          $ip = explode(',', $ip);
          if (in_array($_SERVER['REMOTE_ADDR'], $ip)) {
            $redirect = FALSE;
          }

          if ($redirect) {
            $event->setResponse(new RedirectResponse($baseUrl . '/user/' . $uid . '/twofactor/auth'));
          }
        }
        else {
          // Save interaction time.
          $session->set('twofactor.interaction', time());
        }
      }
    }
  }
}
