<?php

namespace Drupal\twofactor\Commands;

use Drush\Commands\DrushCommands;
use Drupal\entity_usage\EntityUsageBatchManager;

/**
 * Drush commands.
 */
class TwofactorCommands extends DrushCommands {

  /**
   * Disable 2factor.app settings on specific user.
   *
   * @param string $mail
   *   Mail address from user you want disalbe.
   * @command twofactor:disable-user
   * @aliases two-du
   * @usage twofactor:disable-user info@2factor.app
   */
  public function epgProcessing($mail) {

    $user = user_load_by_mail($mail);
    if (!isset($user->uid->value)) {
      drush_log('Error no valid mail', 'error');
      throw new \Exception('Error no valid mail');
    }

    $userData = \Drupal::service('user.data');
    $userData->set('twofactor', $user->id(), 'status', FALSE);
    $this->output()->writeln('User ' . $mail . ' deactivated.');
  }

}

