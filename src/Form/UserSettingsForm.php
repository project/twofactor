<?php
namespace Drupal\twofactor\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;
use Drupal\user\UserData;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UserSettingsForm.
 */
class UserSettingsForm extends FormBase {

  protected $userData;
  protected $account;

  /**
   * Class constructor.
   */
  public function __construct(UserData $user_data, AccountInterface $account) {
    $this->userData = $user_data;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twofactor.user_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {

    $form['api_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API User'),
      '#required' => TRUE,
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'api_user'),
    ];

    $form['api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Password'),
      '#required' => TRUE,
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'api_password'),
    ];

    $form['app_hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('APP Hash'),
      '#required' => TRUE,
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'app_hash'),
    ];

    $form['app_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('APP Password'),
      '#required' => TRUE,
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'app_password'),
    ];

    $form['app_iv'] = [
      '#type' => 'textfield',
      '#title' => $this->t('APP IV'),
      '#required' => TRUE,
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'app_iv'),
    ];

    $form['ip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Whitelisted ip adresses'),
      '#description' => $this->t('Multiple IPs separate by komma. Example: 192.168.1.1,192.168.2.1'),
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'ip'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'status'),
    ];

    $form['register_devices'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Register devices'),
      '#default_value' => $this->userData->get('twofactor', $user->id(), 'register_devices'),
      '#description' => $this->t('If checked, the user must register the device before using the twofactor authentication. Be careful, can be a security impact.'),
    ];

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $user = \Drupal::routeMatch()->getParameter('user');
    if ($user instanceof \Drupal\user\UserInterface) {

      $this->userData->set('twofactor', $user->id(), 'api_user', $form_state->getValue('api_user'));
      $this->userData->set('twofactor', $user->id(), 'api_password', $form_state->getValue('api_password'));
      $this->userData->set('twofactor', $user->id(), 'app_hash', $form_state->getValue('app_hash'));
      $this->userData->set('twofactor', $user->id(), 'app_password', $form_state->getValue('app_password'));
      $this->userData->set('twofactor', $user->id(), 'app_iv', $form_state->getValue('app_iv'));
      $this->userData->set('twofactor', $user->id(), 'ip', $form_state->getValue('ip'));
      $this->userData->set('twofactor', $user->id(), 'status', $form_state->getValue('status'));
      $this->userData->set('twofactor', $user->id(), 'register_devices', $form_state->getValue('register_devices'));

      $uniqueid = md5($form_state->getValue('api_user') . '#' . $form_state->getValue('app_hash') . '#'. $user->id());
      $this->userData->set('twofactor', $user->id(), 'uniqueid', $uniqueid);
    }
  }

}
