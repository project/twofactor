<?php

namespace Drupal\twofactor\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserData;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\SvgWriter;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class AuthenticateTwofactorController.
 *
 * @package Drupal\twofactor\Controller
 */
class AuthenticateTwofactorController extends ControllerBase {

  protected $userData;

  protected $account;

  protected $httpClient;

  protected $currentUser;

  protected $messenger;

  /**
   * Class constructor.
   */
  public function __construct(UserData $user_data, AccountInterface $account, Client $http_client, AccountProxy $current_user, MessengerInterface $messenger) {
    $this->userData = $user_data;
    $this->account = $account;
    $this->httpClient = $http_client;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data'),
      $container->get('current_user'),
      $container->get('http_client'),
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  public function content() {
    // Get request.
    $request = \Drupal::request();

    // Not working for anonymous.
    if ($this->currentUser->id() == 0) {
      return [
        '#markup' => $this->t('Not working for anonymous'),
      ];
    }

    // Get session.
    $session = $request->getSession();

    // Reset session on retry (after create authentication).
    if ($request->query->get('refresh') == 'true') {
      $session->remove('twofactor.code');
      $session->remove('twofactor.request_time');
      $session->remove('twofactor.allowed');
      $session->remove('twofactor.request_status');
      $session->remove('twofactor.app_hash');
      $session->remove('twofactor.request_hash');
      $session->remove('twofactor.request_status');
      $session->remove('twofactor.uniqueid_hasauthentications');

      // Get current full url.
      $current_uri = $request->getUri();

      // Redirect to same url without refresh.
      $reredirect = str_replace('?refresh=true', '', $current_uri);
      $response = new RedirectResponse($reredirect);
      $response->send();
      exit();
    }

    // Check if error is set and show message.
    if ($request->query->get('error')) {
      $error = Xss::filter(base64_decode($request->query->get('error')));
      $this->messenger->addError($this->t('Something went wrong. Message: @message', ['@message' => $error]));

      // Get current full url without get parameters.
      $current_uri = $request->getUri();
      $current_uri = explode('?', $current_uri);
      $current_uri = (isset($current_uri[0]) ? $current_uri[0] : $current_uri);

      $response = new RedirectResponse($current_uri);
      $response->send();
      exit();
    }

    // Check if user has authenticate for this session.
    if ($session->get('twofactor.allowed') == 1) {
      $this->userRedirect();
    }

    // Check if user came back or press retry.
    if ($request->query->get('redirect') == 'back') {
      $this->checkAuthState();
    }

    // Check request time and reset after 120 seconds.
    $counter = 120;
    $request_time = $session->get('twofactor.request_time');
    if ($request_time != '') {
      if ($request_time + 120 < time()) {
        // Reset code, timeout.
        $session->set('twofactor.code', NULL);
      }
      else {
        // Generate counter time.
        $counter = $request_time + 120 - time();
      }
    }

    // Check if code isset.
    if ($session->get('twofactor.code') == NULL) {
      // Generate new code.
      $code = $this->generateRandomString();
      $session->set('twofactor.code', $code);
      $session->set('twofactor.request_time', time());

      // Get user data for twofactor.
      $api_user = $this->userData->get('twofactor', $this->currentUser->id(), 'api_user');
      $api_password = $this->userData->get('twofactor', $this->currentUser->id(), 'api_password');
      $app_hash = $this->userData->get('twofactor', $this->currentUser->id(), 'app_hash');
      $app_password = $this->userData->get('twofactor', $this->currentUser->id(), 'app_password');
      $app_iv = $this->userData->get('twofactor', $this->currentUser->id(), 'app_iv');
      $uniqueid = $this->userData->get('twofactor', $this->currentUser->id(), 'uniqueid');
      $register_devices = $this->userData->get('twofactor', $this->currentUser->id(), 'register_devices');

      // Prepare data for sending.
      $data = [
        'code' => $code,
        'app_hash' => $app_hash,
        'uniqueid' => $uniqueid,
        'email' => $this->account->getEmail(),
        'account_name' => $this->account->getAccountName(),
        'time' => $session->get('twofactor.request_time'),
        'client_ip' => $_SERVER['REMOTE_ADDR'],
      ];

      // Call 2factor.app for new request.
      $request_options = [
        'form_params' => [
          'app_hash' => $app_hash,
          'data' => $this->stringEncode(json_encode($data), $app_password, $app_iv),
        ],
        'headers' => [
          'Authorization' => 'Basic ' . base64_encode($api_user . ':' . $api_password),
        ],
      ];

      $client = $this->httpClient;
      try {
        $response = $client->post('https://2factor.app/api/set_auth', $request_options);
        $response_data = json_decode((string) $response->getBody()->getContents(), TRUE);

        // Error on return.
        if ($response_data['code'] != 100) {
          $session->set('twofactor.allowed', TRUE);
          $session->set('twofactor.request_status', -1);

          $this->messenger->addError($this->t('Something went wrong. Temporary allowed. [Code: @code]', ['@code' => $response_data['code']]));
          $this->userRedirect();
        }

        // Save response data.
        $session->set('twofactor.app_hash', $app_hash);
        $session->set('twofactor.request_hash', $response_data['request_hash']);
        $session->set('twofactor.request_status', $response_data['request_status']);
        $session->set('twofactor.uniqueid_hasauthentications', $response_data['uniqueid_hasauthentications']);
      }
      catch (\Exception $e) {
        $session->set('twofactor.allowed', TRUE);
        $session->set('twofactor.request_status', -1);

        $this->messenger->addError($this->t('Something went wrong. Temporary allowed. [Code: @code]', ['@code' => $e->getMessage()]));
        $this->userRedirect();
      }
    }

    // Get data from tempStore.
    $code = $session->get('twofactor.code');
    $app_hash = $session->get('twofactor.app_hash');
    $request_hash = $session->get('twofactor.request_hash');
    $request_status = $session->get('twofactor.request_status');

    // Security reasons, we ignore this at the moment.
    $uniqueid_hasauthentications = TRUE;
    if ($register_devices) {
      // If set in the user settings, check if the user has already authenticated.
      $uniqueid_hasauthentications = $session->get('twofactor.uniqueid_hasauthentications');
    }

    // Generate URLs.
    $request_uri = base64_encode('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    $check_auth_url = 'https://2factor.app/api/check_auth?app_hash=' . $app_hash . '&request_hash=' . $request_hash;
    $check_auth_url_return = $check_auth_url . '&redirect=' . $request_uri;

    // Generate QRCode with URL.
    $qrCode = QrCode::create($check_auth_url);
    $writer = new SvgWriter();
    $result = $writer->write($qrCode);
    $qrcode_data = $result->getString();

    $return['data'] = [
      '#theme' => 'authenticate',
      '#counter' => $counter,
      '#code' => $code,
      '#check_auth_url_return' => $check_auth_url_return,
      '#qrcode_data' => $qrcode_data,
      '#uniqueid_hasauthentications' => (isset($uniqueid_hasauthentications) && $uniqueid_hasauthentications ? TRUE : FALSE),
      '#attached' => [
        'library' => [
          'twofactor/authenticate',
        ],
      ],
    ];

    return $return;
  }

  /**
   * Redirect user.
   */
  private function userRedirect() {
    $response = new RedirectResponse('/user');
    $response->send();
  }

  /**
   * Generate random string.
   *
   * @param int $length
   *
   * @return string
   */
  private function generateRandomString($length = 6) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      if ($i == ($length / 2)) {
        $randomString .= '-';
      }

      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  /**
   * Encode string for API transfer.
   *
   * @param $string
   * @param string $secret_key
   * @param string $secret_iv
   *
   * @return string
   */
  private function stringEncode($string, $secret_key = '', $secret_iv = '') {
    $encrypt_method = 'AES-256-CBC';
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    return base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
  }

  /**
   * Check authentication state.
   */
  private function checkAuthState() {
    $api_user = $this->userData->get('twofactor', $this->currentUser->id(), 'api_user');
    $api_password = $this->userData->get('twofactor', $this->currentUser->id(), 'api_password');
    $app_hash = $this->userData->get('twofactor', $this->currentUser->id(), 'app_hash');
    $app_password = $this->userData->get('twofactor', $this->currentUser->id(), 'app_password');
    $app_iv = $this->userData->get('twofactor', $this->currentUser->id(), 'app_iv');

    $request = \Drupal::request();
    $session = $request->getSession();

    $request_hash = $session->get('twofactor.request_hash');
    $request_status = $session->get('twofactor.request_status');

    // Prepare data for sending.
    $data = [
      'app_hash' => $app_hash,
      'request_hash' => $request_hash,
      'client_ip' => $_SERVER['REMOTE_ADDR'],
    ];

    // Call 2factor.app for new request.
    $request_options = [
      'form_params' => [
        'app_hash' => $app_hash,
        'data' => $this->stringEncode(json_encode($data), $app_password, $app_iv),
      ],
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode($api_user . ':' . $api_password),
      ],
    ];
    $client = $this->httpClient;

    try {
      $response = $client->post('https://2factor.app/api/get_auth', $request_options);
      $response_data = json_decode((string) $response->getBody()->getContents(), TRUE);

      if ($response_data['code'] == 100 and $response_data['request_status'] == 2) {
        // Save data from response.
        $this->messenger->addStatus($this->t('Verification with 2factor.app successful'));

        $session->set('twofactor.allowed', TRUE);
        $session->set('twofactor.request_hash', $response_data['request_hash']);
        $session->set('twofactor.request_status', $response_data['request_status']);
        $this->userRedirect();
      }
      else {
        $this->messenger->addError($this->t('Not verified yed. Try again.'));
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Something went wrong on retry. Try again. [Code: @code]', ['@code' => $e->getMessage()]));
    }
  }

}
