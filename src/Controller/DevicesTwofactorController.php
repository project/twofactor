<?php

namespace Drupal\twofactor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserData;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\SvgWriter;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class DevicesTwofactorController.
 *
 * @package Drupal\twofactor\Controller
 */
class DevicesTwofactorController extends ControllerBase {

  protected $userData;

  protected $account;

  protected $httpClient;

  protected $currentUser;

  protected $messenger;

  /**
   * Class constructor.
   */
  public function __construct(UserData $user_data, AccountInterface $account, Client $http_client, AccountProxy $current_user, MessengerInterface $messenger) {
    $this->userData = $user_data;
    $this->account = $account;
    $this->httpClient = $http_client;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data'),
      $container->get('current_user'),
      $container->get('http_client'),
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  /**
   * Content.
   *
   * @return array
   *   Return content array.
   */
  public function content() {
    // Get devices for this user.
    $devices = $this->get_devices();

    // Create table header.
    $header = [
      'status' => $this->t('Status'),
      'name' => $this->t('Name'),
      'used' => $this->t('Used'),
      'created' => $this->t('Created'),
      'changed' => $this->t('Changed'),
      'action' => '', // Note yet implemented.
    ];

    // Create table rows.
    $rows = [];
    foreach ($devices as $device) {
      $rows[] = [
        'status' => ($device['status'] == 1 ? $this->t('Active') : $this->t('Inactive')),
        'name' => $device['name'],
        'used' => $device['used'],
        'created' => $device['created'],
        'changed' => $device['changed'],
        'action' => '',
      ];
    }

    // Create table.
    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Get devices.
   *
   * @return array|mixed
   *   Array of devices.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function get_devices() {
    // Generate new code.
    $code = $this->generateRandomString();

    // Get user data for twofactor.
    $api_user = $this->userData->get('twofactor', $this->currentUser->id(), 'api_user');
    $api_password = $this->userData->get('twofactor', $this->currentUser->id(), 'api_password');
    $app_hash = $this->userData->get('twofactor', $this->currentUser->id(), 'app_hash');
    $app_password = $this->userData->get('twofactor', $this->currentUser->id(), 'app_password');
    $app_iv = $this->userData->get('twofactor', $this->currentUser->id(), 'app_iv');
    $uniqueid = $this->userData->get('twofactor', $this->currentUser->id(), 'uniqueid');

    // Prepare data for sending.
    $data = [
      'code' => $code,
      'app_hash' => $app_hash,
      'uniqueid' => $uniqueid,
      'email' => $this->account->getEmail(),
      'account_name' => $this->account->getAccountName(),
      'time' => time(),
      'client_ip' => $_SERVER['REMOTE_ADDR'],
    ];

    // Call 2factor.app for new request.
    $request_options = [
      'form_params' => [
        'app_hash' => $app_hash,
        'data' => $this->stringEncode(json_encode($data), $app_password, $app_iv),
      ],
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode($api_user . ':' . $api_password),
      ],
    ];
    $client = $this->httpClient;

    try {
      $response = $client->post('https://2factor.app/api/set_auth', $request_options);
      $response_data = json_decode((string) $response->getBody()->getContents(), TRUE);
    }
    catch (\Exception $e) {
      $response_data = [
        'status' => 'error',
        'message' => $e->getMessage(),
      ];
    }

    if (isset($response_data['devices'])) {
      return $response_data['devices'];
    }

    return [];
  }

  /**
   * Generate random string.
   *
   * @param int $length
   *   Length of string.
   *
   * @return string
   *   Random string.
   */
  private function generateRandomString($length = 6) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      if ($i == ($length / 2)) {
        $randomString .= '-';
      }

      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  /**
   * Encode string for API transfer.
   *
   * @param $string
   *   String to encode.
   * @param string $secret_key
   *   Secret key.
   * @param string $secret_iv
   *   Secret iv.
   *
   * @return string
   *   Encoded string.
   */
  private function stringEncode($string, $secret_key = '', $secret_iv = '') {
    $encrypt_method = 'AES-256-CBC';
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    return base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
  }

}
